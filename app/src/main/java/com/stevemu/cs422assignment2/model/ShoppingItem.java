package com.stevemu.cs422assignment2.model;

public class ShoppingItem implements Comparable{
    private String desc;
    private double price;
    private int quantity;
    private int priority; // 0 is the highest

    public ShoppingItem(String desc, double price, int quantity, int priority) {
        this.desc = desc;
        this.price = price;
        this.quantity = quantity;
        this.priority = priority;
    }

    public ShoppingItem(ShoppingItem shoppingItem) {
        this(shoppingItem.desc, shoppingItem.price, shoppingItem.quantity, shoppingItem.priority);
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public double getPrice() {
        return price;
    }

    public String getPriceFormatted() {
        return "$" + String.valueOf(price);
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return String.format("%s %.2f %d %d", desc, price, quantity, priority);
    }

    @Override
    public int compareTo(Object o) {
        ShoppingItem other = (ShoppingItem)o;
        if (priority > other.priority) {
            return 1;
        } else if (priority < other.priority) {
            return -1;
        }

        return 0;
    }
}
