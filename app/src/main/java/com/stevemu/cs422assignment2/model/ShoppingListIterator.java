package com.stevemu.cs422assignment2.model;

import java.util.Iterator;

public class ShoppingListIterator<ShoppingItem> implements Iterator<ShoppingItem> {
    ShoppingList list;
    int index;

    public ShoppingListIterator(ShoppingList list) {
        this.list = list;
        index = 0;
    }

    @Override
    public boolean hasNext() {
        if (index == list.size()) {
            return false;
        }
        return true;
    }

    @Override
    public ShoppingItem next() {
        ShoppingItem item = (ShoppingItem) list.get(index);
        index++;
        return item;
    }
}
