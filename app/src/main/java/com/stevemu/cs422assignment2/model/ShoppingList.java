package com.stevemu.cs422assignment2.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ShoppingList implements Iterable<ShoppingItem>{
    private ArrayList<ShoppingItem> shoppingItems;

    public ShoppingList() {
        shoppingItems = new ArrayList<ShoppingItem>();
    }

    public ShoppingList(ArrayList<ShoppingItem> shoppingItems) {
        this.shoppingItems = shoppingItems;
    }

    public void addItem(ShoppingItem newShoppingItem) {

        // check to see if the item is already in the list
        boolean found = false;
        for (ShoppingItem shoppingItem : shoppingItems) {
            if (shoppingItem.getDesc().equals(newShoppingItem.getDesc())) {
                found = true;
            }
        }

        if (found) {
            throw new RuntimeException("You cannot add the same item again.");
        }

        // add to shoppingItems array
        shoppingItems.add(newShoppingItem);

        Collections.sort(shoppingItems);

    }

    public ShoppingItem get(int position) {
        return shoppingItems.get(position);
    }

    public int size() {
        return shoppingItems.size();
    }

    @Deprecated
    public ArrayList<ShoppingItem> getItems() {
        return shoppingItems;
    }

    public static ArrayList<ShoppingItem> getSampleItems() {
        ArrayList<ShoppingItem> shoppingItems = new ArrayList<ShoppingItem>();
        shoppingItems.add(new ShoppingItem("apple", 1,2,1));
        shoppingItems.add(new ShoppingItem("orange", 2,4,2));
        shoppingItems.add(new ShoppingItem("eggplant", 4,2,3));
        shoppingItems.add(new ShoppingItem("eggplant1", 4,2,3));
        shoppingItems.add(new ShoppingItem("eggplant2", 4,2,3));
        shoppingItems.add(new ShoppingItem("eggplant3", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplant4", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplant5", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplant6", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplant8", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplant100", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplant111", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplant112", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplant113", 4,2,3));
//        shoppingItems.add(new ShoppingItem("eggplan114", 4,2,3));
        return shoppingItems;
    }

    public static ShoppingList getSampleList() {
        return new ShoppingList(getSampleItems());
    }

    @Override
    public String toString() {
        String str = "";
        for (ShoppingItem shoppingItem : shoppingItems) {
            str += shoppingItem + "\n";
        }
        return str;
    }

    @Override
    public Iterator<ShoppingItem> iterator() {
        return new ShoppingListIterator<ShoppingItem>(this);
    }


}

