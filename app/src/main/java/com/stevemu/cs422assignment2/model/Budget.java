package com.stevemu.cs422assignment2.model;

public class Budget implements PrintShoppingResultable{

    private double budget = 0;

    private ShoppingList purchasedList;
    private ShoppingList unpurchasedList;
    private ShoppingList originalList;

    public Budget(double budget, ShoppingList list) {
        this.budget = budget;
        this.originalList = list;
    }

    // precondition: budget is set
    // postcondition: purchasedList and unpurchasedList are set
    public void shop() {
        purchasedList = new ShoppingList();
        unpurchasedList = new ShoppingList();

        double budgetLeft = budget;
        for (ShoppingItem shoppingItem : originalList) {

            // copy items and reset quantity
            ShoppingItem shoppingItemWithQuantityPurchased = new ShoppingItem(shoppingItem);
            shoppingItemWithQuantityPurchased.setQuantity(0);
            ShoppingItem shoppingItemWithQuantityUnpurchased = new ShoppingItem(shoppingItem);
            shoppingItemWithQuantityUnpurchased.setQuantity(0);

            for (int i = 0; i < shoppingItem.getQuantity(); i++) {
                double price = shoppingItem.getPrice();
                if (budgetLeft - price < 0) {
                    // cannot buy
                    shoppingItemWithQuantityUnpurchased.setQuantity(shoppingItemWithQuantityUnpurchased.getQuantity()+1);
                } else {
                    // can buy
                    shoppingItemWithQuantityPurchased.setQuantity(shoppingItemWithQuantityPurchased.getQuantity()+1);
                    budgetLeft = budgetLeft - shoppingItem.getPrice();
                }
            }

            if (shoppingItemWithQuantityPurchased.getQuantity() > 0) {
                purchasedList.addItem(shoppingItemWithQuantityPurchased);
            }

            if (shoppingItemWithQuantityUnpurchased.getQuantity() > 0) {
                unpurchasedList.addItem(shoppingItemWithQuantityUnpurchased);
            }


        }

    }

    public ShoppingList getPurchasedList() {
        return purchasedList;
    }

    public ShoppingList getUnpurchasedList() {
        return unpurchasedList;
    }

    public ShoppingList getOriginalList() {
        return originalList;
    }

    public void setOriginalList(ShoppingList originalList) {
        this.originalList = originalList;
    }


    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }
}
