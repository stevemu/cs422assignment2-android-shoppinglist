package com.stevemu.cs422assignment2.model;

import java.util.ArrayList;

public class AppData {
    private static AppData single_instance = null;

    private ShoppingList shoppingList;
    private ShoppingList boughtList;
    private ShoppingList unboughtList;

    private AppData() {
        shoppingList = new ShoppingList();
        boughtList = new ShoppingList();
        unboughtList = new ShoppingList();

    }

    public static AppData getInstance() {
        if (single_instance == null) {
            single_instance = new AppData();
        }
        return single_instance;
    }

    public ShoppingList getShoppingList() {
        return shoppingList;
    }

    public void setShoppingList(ShoppingList shoppingList) {
        this.shoppingList = shoppingList;
    }

    public ShoppingList getBoughtList() {
        return boughtList;
    }

    public void setBoughtList(ShoppingList boughtList) {
        this.boughtList = boughtList;
    }

    public ShoppingList getUnboughtList() {
        return unboughtList;
    }

    public void setUnboughtList(ShoppingList unboughtList) {
        this.unboughtList = unboughtList;
    }
}
