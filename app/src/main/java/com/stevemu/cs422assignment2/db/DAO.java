package com.stevemu.cs422assignment2.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.stevemu.cs422assignment2.model.ShoppingItem;
import com.stevemu.cs422assignment2.model.ShoppingList;

public class DAO {

    private static final DAO ourInstance = new DAO();

    public static DAO getInstance() {
        return ourInstance;
    }

    private DAO() {
    }

    public ShoppingList getUnboughtList(Context c) {
        DbHelper dbHelper = new DbHelper(c);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {"desc", "price", "quantity", "priority"};
        Cursor cursor = db.query("unboughtItems", projection, null, null, null, null, null);

        ShoppingList list = new ShoppingList();
        while (cursor.moveToNext()) {
            String desc = cursor.getString(cursor.getColumnIndexOrThrow("desc"));
            double price = cursor.getDouble(cursor.getColumnIndexOrThrow("price"));
            int quantity = cursor.getInt(cursor.getColumnIndexOrThrow("quantity"));
            int priority = cursor.getInt(cursor.getColumnIndexOrThrow("priority"));
            list.addItem(new ShoppingItem(desc, price, quantity, priority));
        }
        cursor.close();

        return list;
    }

    public void saveUnboughtList(Context c, ShoppingList list) {
        DbHelper dbHelper = new DbHelper(c);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        clearUnboughtTable(c);
        for (ShoppingItem item: list) {
            ContentValues values = new ContentValues();
            values.put("desc", item.getDesc());
            values.put("price", item.getPrice());
            values.put("quantity", item.getQuantity());
            values.put("priority", item.getPriority());
            db.insert("unboughtItems", null, values);
        }
    }

    public void clearUnboughtTable(Context c) {
        DbHelper dbHelper = new DbHelper(c);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("delete from unboughtItems");
    }

}
