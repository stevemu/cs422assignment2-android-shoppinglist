package com.stevemu.cs422assignment2.ui.utilities;

import android.content.Context;
import android.widget.Toast;

public class ViewUtilities {
    public static void showToast(Context context, String text) {
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.TOP, 0,0);
        toast.show();
    }
}
