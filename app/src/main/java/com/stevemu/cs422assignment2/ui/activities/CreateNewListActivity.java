package com.stevemu.cs422assignment2.ui.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.stevemu.cs422assignment2.R;
import com.stevemu.cs422assignment2.db.DAO;
import com.stevemu.cs422assignment2.model.AppData;
import com.stevemu.cs422assignment2.model.Budget;
import com.stevemu.cs422assignment2.model.ShoppingItem;
import com.stevemu.cs422assignment2.model.ShoppingList;
import com.stevemu.cs422assignment2.ui.fragments.ShoppingListFragmentAdapter;
import com.stevemu.cs422assignment2.ui.fragments.ShoppingListFragment;
import com.stevemu.cs422assignment2.ui.utilities.ViewUtilities;

public class CreateNewListActivity extends AppCompatActivity implements ShoppingListFragment.OnListFragmentInteractionListener {

    private Button addNewButton;
    private EditText budgetField;
    private Button startShopButton;
    private Button viewUnboughtButton;
    private RecyclerView shoppingListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_list);

        // set action bar title
        getSupportActionBar().setTitle(R.string.create_list_activity_title);

        addNewButton = findViewById(R.id.addNewButton);
        budgetField = findViewById(R.id.enterBudgetField);
        startShopButton = findViewById(R.id.startShoppingButton);
        shoppingListFragment = findViewById(R.id.shoppingListFragment);
        viewUnboughtButton = findViewById(R.id.viewUnboughtButton);

        ShoppingList data = AppData.getInstance().getShoppingList();
        ShoppingListFragmentAdapter adapter = new ShoppingListFragmentAdapter(data, this);
        shoppingListFragment.setAdapter(adapter);

        addNewButton.setOnClickListener((View view) -> {
            Intent intent = new Intent(this, AddItemActivity.class);
            startActivity(intent);
        });

        viewUnboughtButton.setOnClickListener((View view) -> {
            Intent intent = new Intent(this, UnboughtItemsActivity.class);
            startActivity(intent);
        });

        startShopButton.setOnClickListener((View view) -> {

            if (budgetField.getText().toString().isEmpty()) {
                ViewUtilities.showToast(this, "Please enter budget");
                return;
            }

            double budgetNum = Double.valueOf(budgetField.getText().toString());

            // do shopping
            Budget budget = new Budget(budgetNum, AppData.getInstance().getShoppingList());
            budget.shop();
            AppData.getInstance().setBoughtList(budget.getPurchasedList());
            AppData.getInstance().setUnboughtList(budget.getUnpurchasedList());

            // save to sqlite db
            DAO.getInstance().saveUnboughtList(this, budget.getUnpurchasedList());

            // show result activity
            Intent intent = new Intent(this, ResultActivity.class);
            startActivity(intent);
        });

//        Intent intent = new Intent(this, ResultActivity.class);
//        startActivity(intent);

//        DAO.getInstance().clearUnboughtTable(this);
//        DAO.getInstance().saveUnboughtList(this, ShoppingList.getSampleList());
//        System.out.println(DAO.getInstance().getUnboughtList(this));
    }

    @Override
    public void onListFragmentInteraction(ShoppingItem item) {
//        System.out.println("list here");
//        System.out.println(AppData.getInstance().getShoppingList());
    }

    @Override
    protected void onResume() {
        super.onResume();

        // refresh page
        shoppingListFragment.getAdapter().notifyDataSetChanged();



    }
}
