package com.stevemu.cs422assignment2.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.stevemu.cs422assignment2.R;
import com.stevemu.cs422assignment2.model.AppData;
import com.stevemu.cs422assignment2.model.ShoppingItem;
import com.stevemu.cs422assignment2.ui.utilities.ViewUtilities;

public class AddItemActivity extends AppCompatActivity {

    private EditText descField;
    private EditText priceField;
    private EditText quantityField;
    private EditText priorityField;
    private Button saveButton;

    private String desc;
    private float price;
    private int quantity;
    private int priority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        // set action bar title
        getSupportActionBar().setTitle(R.string.add_item_activity_title);

        descField = findViewById(R.id.descField);
        priceField = findViewById(R.id.priceField);
        quantityField = findViewById(R.id.quantityField);
        priorityField = findViewById(R.id.priorityField);
        saveButton = findViewById(R.id.saveButton);

        TextWatcher watcher = new AddItemActivityTextWatcher();
        descField.addTextChangedListener(watcher);
        priceField.addTextChangedListener(watcher);
        quantityField.addTextChangedListener(watcher);
        priorityField.addTextChangedListener(watcher);

        saveButton.setOnClickListener((View view) -> {

            if (descField.getText().toString().isEmpty() == true || priceField.getText().toString().isEmpty() == true || quantityField.getText().toString().isEmpty() == true || priorityField.getText().toString().isEmpty() == true) {
                ViewUtilities.showToast(this, "Please fill out all fields.");
                return;
            }

            desc = descField.getText().toString();
            price = Float.valueOf(priceField.getText().toString());
            quantity = Integer.valueOf(quantityField.getText().toString());
            priority = Integer.valueOf(priorityField.getText().toString());

            try {
                ShoppingItem item = new ShoppingItem(desc, price, quantity, priority);
                AppData.getInstance().getShoppingList().addItem(item);
                finish();


            } catch (RuntimeException e) {
                ViewUtilities.showToast(this, e.getMessage());
            }


        });
    }

    class AddItemActivityTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            try {
                if (s == descField.getEditableText()) {
                    desc = s.toString();
                } else if (s == priceField.getEditableText()) {
                    price = Float.valueOf(s.toString());
                } else if (s == quantityField.getEditableText()) {
                    quantity = Integer.valueOf(s.toString());
                } else if (s == priorityField.getEditableText()) {
                    priority = Integer.valueOf(s.toString());
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }
    }

}
