package com.stevemu.cs422assignment2.ui.fragments;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stevemu.cs422assignment2.R;
import com.stevemu.cs422assignment2.model.ShoppingItem;
import com.stevemu.cs422assignment2.model.ShoppingList;
import com.stevemu.cs422assignment2.ui.fragments.ShoppingListFragment.OnListFragmentInteractionListener;


public class ShoppingListFragmentAdapter extends RecyclerView.Adapter<ShoppingListFragmentAdapter.ViewHolder> {

    private final ShoppingList list;
    private final OnListFragmentInteractionListener mListener;

    public ShoppingListFragmentAdapter(ShoppingList list, OnListFragmentInteractionListener listener) {
        this.list = list;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_shopping_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mShoppingItem = list.get(position);
        holder.priorityTextView.setText(list.get(position).getPriority() + "");

        String desc = list.get(position).getDesc();
        String price = list.get(position).getPrice() + "";
        String quantity = list.get(position).getQuantity() + "";
        String content = String.format("%s \nPrice: $%s, Quantity: %s", desc, price, quantity);
        holder.mContentView.setText(content);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    Log.d("", "lickced");
//                    mListener.onListFragmentInteraction(holder.mShoppingItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView priorityTextView;
        public final TextView mContentView;
        public ShoppingItem mShoppingItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            priorityTextView = (TextView) view.findViewById(R.id.priorityTextView);
            mContentView = (TextView) view.findViewById(R.id.contentTextView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
