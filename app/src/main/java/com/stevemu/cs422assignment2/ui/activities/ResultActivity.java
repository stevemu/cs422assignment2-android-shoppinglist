package com.stevemu.cs422assignment2.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.stevemu.cs422assignment2.R;
import com.stevemu.cs422assignment2.model.AppData;
import com.stevemu.cs422assignment2.model.ShoppingItem;
import com.stevemu.cs422assignment2.model.ShoppingList;
import com.stevemu.cs422assignment2.ui.fragments.ShoppingListFragment;
import com.stevemu.cs422assignment2.ui.fragments.ShoppingListFragmentAdapter;

public class ResultActivity extends AppCompatActivity implements ShoppingListFragment.OnListFragmentInteractionListener {

    private RecyclerView boughtFragment;
    private RecyclerView unboughtFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        // set action bar title
        getSupportActionBar().setTitle(R.string.result_activity_title);

        boughtFragment = findViewById(R.id.boughtFragment);
        unboughtFragment = findViewById(R.id.unboughtFragment);

        ShoppingList boughtList = AppData.getInstance().getBoughtList();
        boughtFragment.setAdapter(new ShoppingListFragmentAdapter(boughtList, this));

        ShoppingList unboughtList = AppData.getInstance().getUnboughtList();
        unboughtFragment.setAdapter(new ShoppingListFragmentAdapter(unboughtList, this));
    }

    @Override
    public void onListFragmentInteraction(ShoppingItem item) {

    }
}
