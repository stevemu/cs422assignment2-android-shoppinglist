package com.stevemu.cs422assignment2.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.stevemu.cs422assignment2.R;
import com.stevemu.cs422assignment2.db.DAO;
import com.stevemu.cs422assignment2.model.AppData;
import com.stevemu.cs422assignment2.model.ShoppingItem;
import com.stevemu.cs422assignment2.model.ShoppingList;
import com.stevemu.cs422assignment2.ui.fragments.ShoppingListFragment;
import com.stevemu.cs422assignment2.ui.fragments.ShoppingListFragmentAdapter;

public class UnboughtItemsActivity extends AppCompatActivity implements ShoppingListFragment.OnListFragmentInteractionListener {
    private RecyclerView unboughtFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unbought_items);
        unboughtFragment = findViewById(R.id.unboughtFragment);

        getSupportActionBar().setTitle("Unbought Items from last time");

        ShoppingList data = DAO.getInstance().getUnboughtList(this);
        ShoppingListFragmentAdapter adapter = new ShoppingListFragmentAdapter(data, this);
        unboughtFragment.setAdapter(adapter);
    }

    @Override
    public void onListFragmentInteraction(ShoppingItem item) {

    }
}
